//Innacio Tapia Jerez - 20.123.600-2 - Alex Toro - 19.941.255-8
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "universidad.h"
//se definen los prototipos de las funciones que usaremos durante el codigo
void consultarponderacion();
int simularponderacion();
int consultar_facultad();
int entero(char var){
	int numero;
	if(var=="-")return 0;
	numero = atoi(var);
	return numero;
}

void mostrarArchivo(char path[],ponderados aux[]){//funcion para mostrar lo que hay dentro del archivo txt
	FILE *file;//se definen variables 
	int i=0;
	char carrera;
 if((file=fopen(path,"r"))==NULL){//se abre el archivo txt
		printf("\nerror al abirir el archivo\n.");
		exit(0);
	}
	else{
		while(feof(file)==0){//mientras que no llegue al final del archivo se ejecutará la siguiente sentencia, que es printear todos los datos del archivo en pantalla 
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",aux[i].carrera,aux[i].facultad,aux[i].codigo_carrera,aux[i].NEM,aux[i].RANK,aux[i].LENG,aux[i].MAT,aux[i].HIST,aux[i].CS,aux[i].POND,aux[i].PSU1,aux[i].MAX,aux[i].MIN,aux[i].PSU,aux[i].BEA);
			printf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n",aux[i].carrera,aux[i].facultad,aux[i].codigo_carrera,aux[i].NEM,aux[i].RANK,aux[i].LENG,aux[i].MAT,aux[i].HIST,aux[i].CS,aux[i].POND,aux[i].PSU1,aux[i].MAX,aux[i].MIN,aux[i].PSU,aux[i].BEA);
			i++;//aumenta la variable en uno para que el archivo siga avanzando
		}
		fclose(file); //se cierra el archivo
	}

}

void menu(ponderados aux[]){
	int opcion;//variable
    do{
		printf( "\n   1. Consultar ponderacion carrera" );
		printf( "\n   2. Simular postulacion carrera" );
		printf( "\n   3. Mostrar ponderaciones facultad");
		printf( "\n   4. Salir");
		printf( "\n\n   Introduzca opcion (1-4): " );

		scanf( "%d", &opcion );//segun la funcion ingresada se podra entrar al 
		//switch y desde ahi llamar la  funcion para realizar las operaciones

		switch ( opcion ){
			case 1: consultarponderacion(aux);
				break;
			case 2: simularponderacion("ponderados.txt",aux);
				break;
			case 3: consultar_facultad(aux);
				break;
	}

} 

while ( opcion != 4 ); 

}

void consultarponderacion(ponderados aux[]){
	
	char carrera, var;
	int i;//variables
	printf("ingresa la carrera que deseas econtrar : ");
	scanf("%s", &carrera);
	for(i=0;i<54;i++){
		var=aux[i].carrera;
		if(carrera == var){
			printf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",aux[i].carrera,aux[i].facultad,aux[i].codigo_carrera,aux[i].NEM,aux[i].RANK,aux[i].LENG,aux[i].MAT,aux[i].HIST,aux[i].CS,aux[i].POND,aux[i].PSU1,aux[i].MAX,aux[i].MIN,aux[i].PSU,aux[i].BEA);
		}
	}
}

int simularponderacion(char path[], ponderados aux[]){
	int carreraS;
	int punt_NEM, punt_RANK, punt_LENG, punt_MAT, punt_HIST, punt_CS, puntaje_ponderado, Dif_punta, n_e_m, r_a_n_k, l_e_n_g, m_a_t, h_i_s_t, c_s, p_o_n_d;
	int i=0;
	printf("Ingresa el codigo de la carrera: \n");
	scanf("%d", &carreraS);
	while(i<54){
		if(carreraS==atoi(aux[i].codigo_carrera)){

			n_e_m=atoi(aux[i].NEM);
			r_a_n_k=atoi(aux[i].RANK);
			l_e_n_g=atoi(aux[i].LENG);
			m_a_t=atoi(aux[i].MAT);
			h_i_s_t=atoi(aux[i].HIST);
			c_s=atoi(aux[i].CS);
			p_o_n_d=atoi(aux[i].POND);

			printf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n",aux[i].facultad,aux[i].carrera,aux[i].codigo_carrera,aux[i].NEM,aux[i].RANK,aux[i].LENG,aux[i].MAT,aux[i].HIST,aux[i].CS,aux[i].POND,aux[i].PSU1,aux[i].MAX,aux[i].MIN,aux[i].PSU,aux[i].BEA);
		//pedimos al usuarios que ingrese sus puntajes
			printf("Ingrese sus puntajes\n\n");
			printf("Ingresa tu puntaje de NEM: \n");
			scanf("%d\n", &punt_NEM);
			printf("Ingresa tu puntaje de RANK: \n");
			scanf("%d\n", &punt_RANK);
			printf("Ingresa tu puntaje de LENG: \n");
			scanf("%d\n", &punt_LENG);
			printf("Ingresa tu puntaje de MAT: \n");
			scanf("%d\n", &punt_MAT);
			printf("Ingresa tu puntaje de HIST: \n");
			scanf("%d\n", &punt_HIST);
			printf("Ingresa tu puntaje de CS: \n");
			scanf("%d\n", &punt_CS);

			puntaje_ponderado=((punt_NEM*n_e_m)/100+(punt_RANK*r_a_n_k)/100+(punt_LENG*l_e_n_g)/100+(punt_MAT*m_a_t)/100+(punt_HIST*h_i_s_t)/100+(punt_CS*c_s)/100);
			printf("Elpuntaje ponderado obtenido es : %d\n",puntaje_ponderado);
			Dif_punta= (puntaje_ponderado - p_o_n_d);
			printf("La diferencia de tu puntaje ponderado es %d;\n", Dif_punta);
			if(Dif_punta>0)printf("puedes postular a esta carrera\n");
			}
		i++;
	}

	return 0;
}



int consultar_facultad(ponderados aux[]){
	int i;
	FILE *file;
	char Nom_Facu[50];
	file=fopen("ponderados.txt","r");
	printf("Ingrese nombre facultad (intercambie espacios por ',' : ");
	scanf("%s", &Nom_Facu);
	for(i=0;i<74;i++){
		if(strcmp(Nom_Facu,aux[i].facultad)==0);{
			printf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",aux[i].facultad,aux[i].carrera,aux[i].codigo_carrera,aux[i].NEM,aux[i].RANK,aux[i].LENG,aux[i].MAT,aux[i].HIST,aux[i].CS,aux[i].POND,aux[i].PSU1,aux[i].MAX,aux[i].MIN,aux[i].PSU,aux[i].BEA);
		}

	}
	fclose(file);
	return 0;
	

}

	
int main(){
	ponderados aux[164];
	mostrarArchivo("ponderados.txt",aux);
	printf("holaaaa \n");
	menu(aux);

	return  0;
}